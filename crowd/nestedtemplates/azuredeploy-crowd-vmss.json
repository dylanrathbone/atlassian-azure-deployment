{
    "$schema": "https://schema.management.azure.com/schemas/2019-04-01/deploymentTemplate.json#",
    "contentVersion": "1.0.0.0",
    "parameters": {
        "location": {
            "type": "string",
            "metadata": {
                "description": "The region where Azure will house the deployment."
            },
            "minLength": 5
        },
        "_artifactsLocation": {
            "type": "string",
            "metadata": {
                "description": "The URL specifying the location of the installation files.",
                "artifactsBaseUrl": ""
            }
        },
        "_artifactsLocationSasToken": {
            "type": "securestring",
            "defaultValue": "",
            "metadata": {
                "description": "The Shared Access Signature (SAS) token that provides access to resources in the storage account."
            }
        },
        "clusterSize": {
            "type": "int",
            "defaultValue": 2,
            "metadata": {
                "description": "The number of nodes in the Crowd cluster."
            },
            "minValue": 1,
            "maxValue": 6
        },
        "clusterVmSize": {
            "type": "string",
            "defaultValue": "Standard_DS3_v2",
            "allowedValues": [
                "Standard_DS2_v2",
                "Standard_DS3_v2",
                "Standard_DS4_v2",
                "Standard_DS5_v2",
                "Standard_F4s_v2",
                "Standard_F8s_v2",
                "Standard_F16s_v2",
                "Standard_F32s_v2",
                "Standard_F64s_v2",
                "Standard_F72s_v2",
                "Standard_E8s_v3",
                "Standard_E16s_v3",
                "Standard_E20s_v3",
                "Standard_E32s_v3"
            ],
            "metadata": {
                "description": "The size of the virtual machine to use for each Crowd node in the cluster."
            }
        },
        "sshKey": {
            "type": "string",
            "metadata": {
                "description": "The SSH public key to use to access the Jumpbox."
            },
            "minLength": 300
        },
        "sshUserName": {
            "type": "string",
            "defaultValue": "crowdadmin",
            "metadata": {
                "description": "The SSH username to use to access the Crowd nodes from the Jumpbox. Note that this is the only way to access the Crowd nodes."
            }
        },
        "cname": {
            "type": "string",
            "defaultValue": "",
            "metadata": {
                "description": "An existing Canonical Name record (CNAME) to use to access the Crowd instance."
            }
        },
        "dbCreateNew": {
            "type": "bool",
            "defaultValue": true,
            "metadata": {
                "description": "Create a new database or attempt to use an existing specified database. Note that this has to be in same resource group and location as the target deployment."
            }
        },
        "dbType": {
            "type": "string",
            "allowedValues": [
                "sqlserver",
                "postgres",
                "mysql"
            ],
            "defaultValue": "sqlserver",
            "metadata": {
                "description": "The database type."
            }
        },
        "dbPort": {
            "type": "string",
            "defaultValue": "[if(equals(parameters('dbType'), 'sqlserver'), '1433', if(equals(parameters('dbType'), 'postgres'), '5432', '3306'))]",
            "metadata": {
                "description": "The database port to use if an existing Azure database is being used. 1433 for MS SQL, 5432 for PostgreSQL, 3306 for MySQL."
            }
        },
        "dbDatabase": {
            "type": "string",
            "defaultValue": "crowddatabase",
            "metadata": {
                "description": "The name of the Crowd database."
            }
        },
        "dbSchema": {
            "type": "string",
            "defaultValue": "[if(parameters('dbCreateNew'), if(equals(parameters('dbType'), 'sqlserver'), 'crowdschema', 'public'), 'crowdschema')]",
            "metadata": {
                "description": "The Crowd database schema to use."
            }
        },
        "dbUsername": {
            "type": "string",
            "defaultValue": "crowd",
            "metadata": {
                "description": "The username for the dedicated database user."
            }
        },
        "dbPassword": {
            "type": "securestring",
            "minLength": 8,
            "metadata": {
                "description": "The password for the dedicated database user."
            }
        },
        "crowdVersion": {
            "type": "string",
            "defaultValue": "latest",
            "metadata": {
                 "description": "The Crowd software product version to install. By default the deployment will download and use the latest version published."
             }
         },
         "customDownloadUrl": {
            "type": "string",
            "defaultValue": "",
            "metadata": {
                 "description": "Use this URL to override standard Atlassian download url eg for EAP, RC versions. NB will be used in conjunction with the crowdVersion parameter."
             }
         },
        "crowdAdminUserName": {
            "type": "string",
            "defaultValue": "admin",
            "minLength": 5,
            "metadata": {
                "description": "The username for the Crowd administrator's account."
            }
        },
        "crowdAdminUserPassword": {
            "type": "securestring",
            "minLength": 5,
            "metadata": {
                "description": "The password for the Crowd administrator's account."
            }
        },
        "crowdAdminUserFullname": {
            "type": "string",
            "defaultValue": "Admin Admin",
            "minLength": 5,
            "metadata": {
                "description": "The fullname for the Crowd administrator's account."
            }
        },
        "crowdAdminUserEmail": {
            "type": "string",
            "defaultValue": "admin@example.com",
            "minLength": 5,
            "metadata": {
                "description": "The email address of the Crowd administrator user."
            }
        },
        "crowdAppTitle": {
            "type": "string",
            "defaultValue": "Atlassian Crowd",
            "metadata": {
                "description": "The name of the Crowd site."
            }
        },
        "vnetName": {
            "type": "string",
            "metadata": {
                "description": "The name of the Crowd virtual network."
            }
        },
        "appSubnetName": {
            "type": "string",
            "metadata": {
                "description": "The name of the subnet within the Crowd virtual network where the Crowd cluster is located."
            }
        },
        "jumpboxName": {
            "type": "string",
            "metadata": {
                "description": "The name of the Jumpbox."
            }
        },
        "sslBase64EncodedPfxCertificate": {
            "type": "string",
            "defaultValue": "",
            "metadata": {
                "description": "The certificate to be used for SSL termination on the Azure Application Gateway."
            }
        },
        "appInsightsInstrumentKey": {
            "type": "string",
            "defaultValue": "",
            "metadata": {
              "description": "The Application Insights instrumentation key."
            }
        },
        "gtwyName": {
            "type": "string",
            "metadata": {
              "description": "The name of the Application Gateway."
            }
        },
        "gtwyAddressPoolName": {
            "type": "string",
            "metadata": {
              "description": "The name of the Application Gateway's address pool."
            }
        },
        "crowdBaseUrl": {
            "type": "string",
            "metadata": {
                "description": "The base URL of the Crowd instance."
            }
        },
        "crowdDbServerName": {
            "type": "string",
            "metadata": {
                "description": "The Crowd database server name."
            }
        },
        "storageAccountName": {
            "type": "string",
            "metadata": {
                "description": "The storage account name."
            }
        },
        "sharedHomeName": {
            "type": "string",
            "metadata": {
                "description": "The shared home file share name."
            }
        },
        "storageAccessKey": {
            "type": "securestring",
            "metadata": {
                "description": "The storage access key used to authorize access to the storage account."
            }
        },
        "enableAcceleratedNetworking": {
            "type": "bool",
            "defaultValue": true,
            "metadata": {
                "description": "Enable accelerated networking. Note that this needs to be on a supported instance type."
            }
        },
        "workspaceId": {
            "type": "string",
            "metadata": {
                "description": "The ID of the Azure Operations Management Suite (OMS) workspace."
            }
        },
        "workspacePrimaryKey": {
            "type": "securestring",
            "metadata": {
                "description": "The primary key of the Azure Operations Management Suite (OMS) workspace."
            }
        },
        "workspaceSecondaryKey": {
            "type": "securestring",
            "metadata": {
                "description": "The secondary key of the Azure Operations Management Suite (OMS) workspace."
            }
        },
        "clusterVmDiskSize": {
            "type": "int",
            "defaultValue": 1000,
            "metadata": {
                "description": "The desired size of the Cluster Node's allocation disk in GB"
            }
        },
        "clusterVmDiskCount": {
            "type": "int",
            "defaultValue": 8,
            "minValue": 8,
            "maxValue": 32,
            "metadata": {
                "description": "The requested Cluster Node disk will be striped for performance. Essentially the desired disk size will be divided by the clusterVmDiskCount and can't exceed 1023 GB.. The clusterVmDiskCount count can't exceed the MaxDiskCount assigned to the VM Size."
            }
        },
        "linuxOsType": {
            "type": "string",
            "defaultValue": "Canonical:UbuntuServer:18.04-LTS",
            "allowedValues": [
                "Canonical:UbuntuServer:16.04-LTS",
                "Canonical:UbuntuServer:18.04-LTS",
                "RedHat:RHEL:7.5",
                "OpenLogic:CentOS:7.5",
                "credativ:Debian:9-backports"
            ],
            "metadata": {
                "description": "Select your preferred Linux OS type. The selected Linux OS type has to support Accelerated Networking as well - https://docs.microsoft.com/en-us/azure/virtual-network/create-vm-accelerated-networking-cli"
            }
        },
        "diagnosticsBlobStorageUri": {
            "type": "string"
        }
    },
    "variables": {
        "namespace": "crowd",
        "isSslSpecified": "[not(empty(parameters('sslBase64EncodedPfxCertificate')))]",
        "env": {
            "storage": [
                {
                    "name": "STORAGE_BASE_URL",
                    "value": "[if(endsWith(parameters('_artifactsLocation'), '/'), parameters('_artifactsLocation'), concat(parameters('_artifactsLocation'), '/'))]"
                },
                {
                    "name": "STORAGE_TOKEN",
                    "value": "[parameters('_artifactsLocationSasToken')]"
                },
                {
                    "name": "STORAGE_ACCOUNT",
                    "value": "[parameters('storageAccountName')]"
                }
            ],
            "server": [
                {
                    "name": "SERVER_APP_PORT",
                    "value": "8080"
                },
                {
                    "name": "SERVER_CNAME",
                    "value": "[parameters('cname')]"
                },
                {
                    "name": "SERVER_PROXY_PORT",
                    "value": "[if(variables('isSslSpecified'), '443', '80')]"
                },
                {
                    "name": "SERVER_APP_SCHEME",
                    "value": "[if(variables('isSslSpecified'), 'https', 'http')]"
                },
                {
                    "name": "SERVER_CLUSTER_NAME",
                    "value": "crowdcluster"
                },
                {
                    "name": "SERVER_CATALINA_HOME",
                    "value": "/catalina/home/placeholder"
                },
                {
                    "name": "SERVER_SECURE_FLAG",
                    "value": "[if(variables('isSslSpecified'), 'true', 'false')]"
                },
                {
                    "name": "SERVER_SSH_USER",
                    "value": "[parameters('sshUserName')]"
                },
                {
                    "name": "APPINSIGHTS_VER",
                    "value": "2.3.0"
                },
                {
                    "name": "OMS_WORKSPACE_ID",
                    "value": "[parameters('workspaceId')]"
                },
                {
                    "name": "OMS_PRIMARY_KEY",
                    "value": "[parameters('workspacePrimaryKey')]"
                },
                {
                    "name": "OMS_SECONDARY_KEY",
                    "value": "[parameters('workspaceSecondaryKey')]"
                }
            ],
            "db": [
                {
                    "name": "DB_USER",
                    "value": "[parameters('dbUsername')]"
                },
                {
                    "name": "DB_PASSWORD",
                    "value": "[parameters('dbPassword')]"
                },
                {
                    "name": "DB_NAME",
                    "value": "[if(equals(parameters('dbType'), 'sqlserver'), parameters('dbDatabase'), toLower(parameters('dbDatabase')))]"
                },
                {
                    "name": "DB_SCHEMA",
                    "value": "[if(equals(parameters('dbType'), 'sqlserver'), parameters('dbSchema'), toLower(parameters('dbSchema')))]"
                },
                {
                    "name": "DB_PORT",
                    "value": "[if(parameters('dbCreateNew'), if(equals(parameters('dbType'), 'sqlserver'), '1433', '5432'), parameters('dbPort'))]"
                },
                {
                    "name": "DB_TYPE",
                    "value": "[parameters('dbType')]"
                },
                {
                    "name": "DB_CREATE",
                    "value": "[parameters('dbCreateNew')]"
                }
            ],
            "sql": [
                {
                    "name": "USER_NAME",
                    "value": "[parameters('crowdAdminUserName')]"
                },
                {
                    "name": "USER_NAME_LOWERCASE",
                    "value": "[toLower(parameters('crowdAdminUserName'))]"
                },
                {
                    "name": "USER_FULLNAME",
                    "value": "[parameters('crowdAdminUserFullname')]"
                },
                {
                    "name": "USER_FULLNAME_LOWERCASE",
                    "value": "[toLower(parameters('crowdAdminUserFullname'))]"
                },
                {
                    "name": "USER_EMAIL",
                    "value": "[parameters('crowdAdminUserEmail')]"
                },
                {
                    "name": "USER_EMAIL_LOWERCASE",
                    "value": "[toLower(parameters('crowdAdminUserEmail'))]"
                },
                {
                    "name": "USER_CREDENTIAL",
                    "value": "[parameters('crowdAdminUserPassword')]"
                },
                {
                    "name": "CROWD_LICENSE",
                    "value": "[variables('crowd').license]"
                },
                {
                    "name": "APPLICATION_TITLE",
                    "value": "[parameters('crowdAppTitle')]"
                }
            ],
            "settings": [
                {
                    "name": "ATL_CROWD_HOME",
                    "value": "/var/atlassian/application-data/crowd"
                },
                {
                    "name": "ATL_CROWD_SHARED_HOME",
                    "value": "/media/atl/crowd/shared"
                },
                {
                    "name": "ATL_CROWD_INSTALL_DIR",
                    "value": "/opt/atlassian/crowd"
                },
                {
                    "name": "ATL_CROWD_SHARED_HOME_NAME",
                    "value": "[parameters('sharedHomeName')]"
                },
                {
                    "name": "ATL_CROWD_RELEASES_BASE_URL",
                    "value": "https://s3.amazonaws.com/atlassian-software/releases"
                },
                {
                    "name": "ATL_CROWD_PRODUCT",
                    "value": "crowd"
                },
                {
                    "name": "ATL_CROWD_PRODUCT_VERSION",
                    "value": "[parameters('crowdVersion')]"
                },
                {
                    "name": "ATL_CROWD_CUSTOM_DOWNLOAD_URL",
                    "value": "[parameters('customDownloadUrl')]"
                }
            ]
        },
        "crowd": {
            "cluster": {
                "name": "[concat(variables('namespace'), 'cluster')]",
                "tier": "Standard",
                "vm": {
                    "size": "[parameters('clusterVmSize')]",
                    "image": {
                        "publisher": "[split(parameters('linuxOsType'), ':')[0]]",
                        "offer": "[split(parameters('linuxOsType'), ':')[1]]",
                        "sku": "[split(parameters('linuxOsType'), ':')[2]]",
                        "version": "latest"
                    },
                    "osdisk": {
                        "type": "Premium_LRS",
                        "size": "100"
                    },
                    "datadisk": {
                        "type": "Premium_LRS",
                        "caching": "ReadWrite",
                        "size": "[if(greater(div(parameters('clusterVmDiskSize'), parameters('clusterVmDiskCount')), 1020), 1020, div(parameters('clusterVmDiskSize'), parameters('clusterVmDiskCount')))]"
                    }
                }
            },
            "license": "TODO-CROWD: THIS NEEDS TO BE UPDATED TO A CROWD LICENCE, CURRENTLY THIS IS A JIRA LICENCE. AAAB8A0ODAoPeNp9Ul1vmzAUfedXWNrLFgmSknaVIiEtA2/L1EAEZB9a9+CYm8QtsS3btMt+/Yyh6gddEDyY63PuueeeN9+hQgVIFF6iSTg7n87OL9DnZYnCydmlt1MAfC+kBBVcMQpcA66YYYJHOC1xvsoXBfbS5rABlW3XGpSOwgsvFtwQalJygEg3UgplPhBTE60Z4QEVB++GKRIMcKtG0T3RkBADUSvAn5zZ1+tbl0cJjjPOlkucx4v51UMJ/5FMHR1uNZ2+//IgAS8Jq09pkEpUDTVBe/C12Jp7oiCwSHYHkVENnLpm+5EYuAHVXS2ajaaKSWeQ+/OKga9N4Xr83+DRaJRmpf8py/1VniXruFxkqb8usC1EsQI7doU2R2T2gHoWhDkVFShkld8ANejX3hh5PRuPdyJ45sO47hA+dIjfAUoE4sKgimmj2KYxYJmZRkYg2mgjDnZdgWedtZNzwunQfasrzvG8xIn/8WcrkoA6iBr4rXixhF7uIonW/JaLe37K70Fg3N3BFnAa2c//4Z5TfIUhqoVtSa3By9SOcKaJc7+ypD51rD752yjwnM+29DKcvX3frKQWGD7buSOQiuk+TAk8BuTrIp+joteC3rZjoG6Od9czhO9I3biG3QSDSJ6I01MFT3GPnN35Hw9baBMwLAIUarbD2ZcwPRpegxJGdSMJtiQjn/QCFGulFcz4pDzNWDZGR6RTwSNU/ARaX02nf"
        },
        "data": {
            "storage": "[concat(' STORAGE_ATL_ENV_DATA=\"', base64(string(variables('env').storage)), '\" ')]",
            "server": "[concat(' SERVER_ATL_ENV_DATA=\"', base64(string(variables('env').server)), '\" ')]",
            "db": "[concat(' DB_ATL_ENV_DATA=\"', base64(string(variables('env').db)), '\" ')]",
            "sql": "[concat(' SQL_ATL_ENV_DATA=\"', base64(string(variables('env').sql)), '\" ')]",
            "settings": "[concat(' SETTINGS_ATL_ENV_DATA=\"', base64(string(variables('env').settings)), '\" ')]"
        },
        "tags": {
            "vendor": "Atlassian",
            "product": "CROWD",
            "provider": "2F0AF47A-92C7-413A-9009-C3815BFF7AF6"
        },
        "scripts": {
            "prepare": {
                "name": "prepare_install.sh",
                "env": "[concat(variables('data').storage, variables('data').server, variables('data').db, variables('data').sql, variables('data').settings)]"
            }
        },
        "crowdPrepareCmd": "[concat(variables('scripts').prepare.env, './prepare_install.sh ', parameters('storageAccessKey'), ' prepare ', parameters('crowdBaseUrl'), ' ', parameters('crowdDbServerName'), ' ', if(variables('isSslSpecified'), 'https', 'http'), ' ', if(not(empty(parameters('appInsightsInstrumentKey'))), parameters('appInsightsInstrumentKey'), ' '), ' 1>&1 2>&1 > ./crowd.prepare.log')]",
        "crowdInstallCmd": "[concat(variables('scripts').prepare.env, './prepare_install.sh ', parameters('storageAccessKey'), ' install ', if(not(empty(parameters('appInsightsInstrumentKey'))), parameters('appInsightsInstrumentKey'), ' '), ' 1>&1 2>&1 > ./crowd.install-$(uname -n).log')]",
        "isMsSql": "[equals(parameters('dbType'), 'sqlserver')]",
        "crowdDbScriptTemplate": "[if(variables('isMsSql'), 'templates/crowd_db.sql.template', 'templates/crowd_postgres_db.sql.template')]",
        "dbScriptTemplate": "[variables('crowdDbScriptTemplate')]"
    },
    "resources": [
        {
            "type": "Microsoft.Compute/virtualMachines/extensions",
            "apiVersion": "2018-06-01",
            "name": "[concat(parameters('jumpboxName'), '/prepare-install-script')]",
            "location": "[parameters('location')]",
            "tags": "[variables('tags')]",
            "properties": {
                "publisher": "Microsoft.Azure.Extensions",
                "type": "CustomScript",
                "typeHandlerVersion": "2.0",
                "autoUpgradeMinorVersion": true,
                "settings": {
                    "fileUris": [
                        "[uri(parameters('_artifactsLocation'), concat('scripts/prepare_install.sh', parameters('_artifactsLocationSasToken')))]",
                        "[uri(parameters('_artifactsLocation'), concat('scripts/hydrate_crowd_config.py', parameters('_artifactsLocationSasToken')))]",
                        "[uri(parameters('_artifactsLocation'), concat('templates/dbconfig.xml.template', parameters('_artifactsLocationSasToken')))]",
                        "[uri(parameters('_artifactsLocation'), concat(variables('dbScriptTemplate'), parameters('_artifactsLocationSasToken')))]",
                        "[uri(parameters('_artifactsLocation'), concat('templates/databaseChangeLog.xml.template', parameters('_artifactsLocationSasToken')))]",
                        "[uri(parameters('_artifactsLocation'), concat('templates/server.xml.template', parameters('_artifactsLocationSasToken')))]",
                        "[uri(parameters('_artifactsLocation'), concat('templates/ApplicationInsights.xml.template', parameters('_artifactsLocationSasToken')))]",
                        "[uri(parameters('_artifactsLocation'), concat('templates/crowd-collectd.conf.template', parameters('_artifactsLocationSasToken')))]"
                    ]
                },
                "protectedSettings": {
                    "commandToExecute": "[variables('crowdPrepareCmd')]"
                }
            }
        },
        {
            "type": "Microsoft.Compute/virtualMachineScaleSets",
            "name": "[variables('crowd').cluster.name]",
            "location": "[parameters('location')]",
            "apiVersion": "2018-06-01",
            "dependsOn": [
                "[concat('Microsoft.Compute/virtualMachines/',parameters('jumpboxName'),'/extensions/prepare-install-script')]"
            ],
            "sku": {
                "name": "[variables('crowd').cluster.vm.size]",
                "tier": "[variables('crowd').cluster.tier]",
                "capacity": "[parameters('clusterSize')]"
            },
            "properties": {
                "overprovision": false,
                "singlePlacementGroup": false,
                "upgradePolicy": {
                    "mode": "Manual"
                },
                "virtualMachineProfile": {
                    "osProfile": {
                        "computerNamePrefix": "crowdnode-",
                        "adminUsername": "[parameters('sshUserName')]",
                        "linuxConfiguration": {
                            "disablePasswordAuthentication": true,
                            "ssh": {
                                "publicKeys": [
                                    {
                                        "path": "[concat('/home/', parameters('sshUserName'), '/.ssh/authorized_keys')]",
                                        "keyData": "[parameters('sshKey')]"
                                    }
                                ]
                            }
                        }
                    },
                    "storageProfile": {
                        "imageReference": {
                            "publisher": "[variables('crowd').cluster.vm.image.publisher]",
                            "offer": "[variables('crowd').cluster.vm.image.offer]",
                            "sku": "[variables('crowd').cluster.vm.image.sku]",
                            "version": "[variables('crowd').cluster.vm.image.version]"
                        },
                        "osDisk": {
                            "managedDisk": {
                                "storageAccountType": "[variables('crowd').cluster.vm.osdisk.type]"
                            },
                            "diskSizeGB": "[variables('crowd').cluster.vm.osdisk.size]",
                            "createOption": "FromImage"
                        },
                       "copy": [
                            {
                                "name": "dataDisks",
                                "count": "[parameters('clusterVmDiskCount')]",
                                "input": {
                                    "lun": "[copyIndex('dataDisks')]",
                                    "caching": "[variables('crowd').cluster.vm.datadisk.caching]",
                                    "createOption": "Empty",
                                    "diskSizeGB": "[variables('crowd').cluster.vm.datadisk.size]",
                                    "managedDisk": {
                                        "storageAccountType": "[variables('crowd').cluster.vm.datadisk.type]"
                                    }
                                }
                            }
                        ]
                    },
                    "networkProfile": {
                        "networkInterfaceConfigurations": [
                            {
                                "name": "[concat(variables('namespace'), 'scaleset-nic')]",
                                "properties": {
                                    "primary": true,
                                    "enableAcceleratedNetworking": "[parameters('enableAcceleratedNetworking')]",
                                    "ipConfigurations": [
                                        {
                                            "name": "[concat(variables('namespace'), 'scalset-ipconfig')]",
                                            "properties": {
                                                "subnet": {
                                                    "id": "[resourceId('Microsoft.Network/virtualNetworks/subnets/', parameters('vnetName'), parameters('appSubnetName'))]"
                                                },
                                                "ApplicationGatewayBackendAddressPools": [
                                                    {
                                                        "id": "[resourceId('Microsoft.Network/applicationGateways/backendAddressPools', parameters('gtwyName'), parameters('gtwyAddressPoolName'))]"
                                                    }
                                                ]
                                            }
                                        }
                                    ]
                                }
                            }
                        ]
                    },
                    "diagnosticsProfile": {
                        "bootDiagnostics": {
                            "enabled": true,
                            "storageUri": "[parameters('diagnosticsBlobStorageUri')]"
                        }
                    },
                    "extensionProfile": {
                        "extensions": [
                            {
                                "name": "crowd-install-script",
                                "properties": {
                                    "publisher": "Microsoft.Azure.Extensions",
                                    "type": "CustomScript",
                                    "typeHandlerVersion": "2.0",
                                    "autoUpgradeMinorVersion": true,
                                    "settings": {
                                        "fileUris": [
                                            "[uri(parameters('_artifactsLocation'), concat('scripts/prepare_install.sh', parameters('_artifactsLocationSasToken')))]",
                                            "[uri(parameters('_artifactsLocation'), concat('scripts/appinsights_transform_web_xml.xsl', parameters('_artifactsLocationSasToken')))]",
                                            "[uri(parameters('_artifactsLocation'), concat('scripts/vm-disk-utils-0.1.sh', parameters('_artifactsLocationSasToken')))]"
                                        ]
                                    },
                                    "protectedSettings": {
                                        "commandToExecute": "[variables('crowdInstallCmd')]"
                                    }
                                }
                            }
                        ]
                    }
                }
            }
        }
    ],
    "outputs": {
        "clusterName": {
            "type": "string",
            "value": "[variables('crowd').cluster.name]"
        },
        "prepareCmdArgs": {
            "type": "string",
            "value": "[variables('crowdPrepareCmd')]"
        },
        "installCrowdCmdArgs": {
            "type": "string",
            "value": "[variables('crowdInstallCmd')]"
        },
        "vmmsSingleDiskSizeGB": {
            "type": "int",
            "value": "[variables('crowd').cluster.vm.datadisk.size]"
        }
    }
}